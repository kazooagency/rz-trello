import React from 'react';

import { StyleSheet, css } from 'aphrodite';

function Card(props) {
   const { text, onClick } = props;
   return (
      <div onClick={onClick} className={css(styles.card)}>
         <p>{text}</p>
      </div>
   );
}

export default Card;

const styles = StyleSheet.create({
   card: {
      backgroundColor: 'white',
      width: '100%',
      padding: '6px 8px 2px',
      borderRadius: '3px',
      boxShadow: '0 1px 0 rgba(9,30,66,.25)',
      cursor: 'pointer',
      marginBottom: '8px',
      maxWidth: '300px',
      minHeight: '20px',
      color: '#172b4d',
   },
});
