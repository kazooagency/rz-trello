import React from 'react';
import ReactDOM from 'react-dom';
import { Editor } from 'draft-js';

function MyEditor(props) {
   return <Editor {...props} />;
}

export default MyEditor;
