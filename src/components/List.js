import React from 'react';
import Card from './Card';
import AddButton from './AddButton';
import CloseIcon from '@material-ui/icons/Close';

import { StyleSheet, css } from 'aphrodite';

function List(props) {
   const { title, cards, deleteList, cardOnClick, listID } = props;
   return (
      <div className={css(styles.list)}>
         <div className={css(styles.titleAndButtonContainer)}>
            <h2 className={css(styles.title)}>{title}</h2>
            <CloseIcon onMouseDown={() => deleteList()} />
         </div>
         {cards.map(card => (
            <Card text={card.text} onClick={() => cardOnClick(card.id)} />
         ))}
      </div>
   );
}

export default List;

const styles = StyleSheet.create({
   titleAndButtonContainer: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'space-between',
   },
   title: {
      fontSize: '14px',
      marginBottom: '12px',
   },
});
