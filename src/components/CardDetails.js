import React, { useState } from 'react';
import { StyleSheet, css } from 'aphrodite';

import DialogTitle from '@material-ui/core/DialogTitle';
import Dialog from '@material-ui/core/Dialog';
import Typography from '@material-ui/core/Typography';
import TextareaAutosize from '@material-ui/core/TextareaAutosize';

import { EditorState, convertToRaw, convertFromRaw } from 'draft-js';
import Editor from './Editor';

import Edit from '@material-ui/icons/Edit';

function CardDetails(props) {
   const { isOpen, setIsOpen, title, description, onClose, cardId, onValidate } = props;
   let editorStateruu;
   if (description.length > 0) {
      editorStateruu = EditorState.createWithContent(convertFromRaw(JSON.parse(description)));
   } else {
      editorStateruu = EditorState.createEmpty();
   }

   const [editorState, setEditorState] = React.useState(editorStateruu);

   const [newDescriptionValue, setDescriptionValue] = useState(undefined);
   const [isTextareaOpen, setIsTextareaOpen] = useState(false);
   const handleEditClick = () => {
      setIsTextareaOpen(true);
   };

   const handleTextareaChange = e => {
      console.log(e);
      setDescriptionValue(e);
   };

   const handleLocalValidate = () => {
      setIsTextareaOpen(false);
      onValidate({
         id: cardId,
         newDescriptionValue: JSON.stringify(convertToRaw(editorState.getCurrentContent())),
      });
   };

   const cleanAndClose = () => {
      onClose();
      setIsTextareaOpen(false);

      setTimeout(() => {
         setDescriptionValue(undefined);
      }, 200);
   };

   return (
      <Dialog onClose={cleanAndClose} aria-labelledby="simple-dialog-title" open={isOpen}>
         <DialogTitle id="simple-dialog-title">{title}</DialogTitle>
         {isTextareaOpen ? null : (
            <div className={css(styles.listAndAddButtonConter)}>
               <Editor
                  readOnly
                  onChange={setEditorState}
                  editorState={editorState}
                  setEditorState={setEditorState}
               />
               <button onClick={() => handleEditClick()}>
                  <Edit />
               </button>
            </div>
         )}

         {isTextareaOpen ? (
            <div>
               <Editor
                  autofocus
                  onChange={setEditorState}
                  editorState={editorState}
                  setEditorState={setEditorState}
               />
               <button onClick={handleLocalValidate}>Validate</button>
            </div>
         ) : null}
      </Dialog>
   );
}

export default CardDetails;

const styles = StyleSheet.create({
   listAndAddButtonConter: {
      display: 'flex',
      flexDirection: 'row',
   },
});
