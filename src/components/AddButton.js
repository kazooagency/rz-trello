import React, { useState } from 'react';
import TextareaAutosize from '@material-ui/core/TextareaAutosize';
import Button from '@material-ui/core/Button';
import CloseIcon from '@material-ui/icons/Close';
import { withStyles } from '@material-ui/core/styles';

import { StyleSheet, css } from 'aphrodite';

function AddButton(props) {
   const { type, another, action } = props;

   const [isOpen, setIsOpen] = useState(false);
   const [textAreaValue, settextAreaValue] = useState('');

   const handleOnclickAdd = () => setIsOpen(true);

   const handleTextareaChange = e => {
      settextAreaValue(e.target.value);
   };

   const handleOnKeyDown = e => {
      if (e.keyCode === 13) {
         e.preventDefault();
         handleValidate();
      }
   };

   const handleValidate = () => {
      if (textAreaValue) {
         action(textAreaValue);
      }
      setIsOpen(false);
      settextAreaValue('');
   };
   const handleOnBlur = () => {
      setIsOpen(false);
      if (type === 'card') {
         handleValidate();
      } else {
         settextAreaValue('');
      }
   };

   const StyledButton = withStyles({
      root: {
         height: '38px',
         width: '258px',
         textAlign: 'left',
         boxShadow: 'none',
         textTransform: 'none',
         fontSize: 16,
         backgroundColor: 'transparent',
         color: '#172b4d',
         '&:hover': {
            backgroundColor: 'rgba(9,30,66,.08)',
            boxShadow: 'none',
         },
         '&:active': {
            boxShadow: 'none',
            backgroundColor: 'rgba(9,30,66,.08)',
         },
         '&:focus': {
            boxShadow: '0 0 0 0.2rem rgba(0,123,255,.5)',
         },
      },
   })(Button);

   return isOpen ? (
      <div className={css(styles.inputAndButtonContainer)}>
         <TextareaAutosize
            autoFocus
            aria-label="textarea"
            placeholder="Empty"
            onChange={handleTextareaChange}
            onKeyDown={handleOnKeyDown}
            value={textAreaValue}
            onBlur={handleOnBlur}
         />
         <div className={css(styles.buttonAndIconContainer)}>
            <StyledButton variant="contained" color="primary" onMouseDown={handleValidate}>
               {`Add ${type}`}
            </StyledButton>
            <CloseIcon onMouseDown={() => settextAreaValue('')} />
         </div>
      </div>
   ) : (
      <StyledButton variant="contained" color="primary" onClick={handleOnclickAdd}>
         Add {another ? 'another ' : null}
         {type}
      </StyledButton>
   );
}

export default AddButton;

const styles = StyleSheet.create({
   inputAndButtonContainer: {
      display: 'flex',
      flexDirection: 'column',
   },
   buttonAndIconContainer: {
      display: 'flex',
      flexDirection: 'row',
   },
});
