import ACTION_TYPES from './actionTypes';

export const addList = listTitle => {
   return {
      type: ACTION_TYPES.LISTS.ADD_LIST,
      listTitle,
   };
};

export const addCard = (listId, text) => {
   console.log('addcard listID', listId);
   return {
      type: ACTION_TYPES.CARDS.ADD_CARD,
      text,
      listId,
   };
};

export const deleteList = listId => {
   return {
      type: ACTION_TYPES.LISTS.DELETE_LIST,
      listId,
   };
};

export const updateCard = card => {
   return {
      type: ACTION_TYPES.CARDS.UPDATE_CARD,
      card,
   };
};
