import ACTION_TYPES from './actionTypes';

import { v4 as uuidv4 } from 'uuid';

const trelloApp = (state = [], action) => {
   switch (action.type) {
      case ACTION_TYPES.LISTS.ADD_LIST: {
         return [
            ...state,
            {
               id: uuidv4(),
               title: action.listTitle,
               cards: [],
            },
         ];
      }
      case ACTION_TYPES.CARDS.ADD_CARD: {
         const newState = [...state];
         newState.map(list => {
            if (list.id === action.listId) {
               list.cards.push({ id: uuidv4(), text: action.text, description: '' });
            }
         });
         return newState;
      }
      case ACTION_TYPES.LISTS.DELETE_LIST: {
         const newState = [...state];
         newState.splice(
            newState.findIndex(el => el.id === action.listId),
            1,
         );
         return newState;
      }
      case ACTION_TYPES.CARDS.UPDATE_CARD: {
         const newState = [...state];
         console.log('update card action', action);
         newState.map(list =>
            list.cards.map(card => {
               if (card.id === action.card.id) {
                  card.description = action.card.newDescriptionValue;
               }
            }),
         );

         return newState;
      }
      default:
         return state;
   }
};

export default trelloApp;
