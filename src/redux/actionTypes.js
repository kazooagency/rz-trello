const ACTION_TYPES = {
   LISTS: {
      ADD_LIST: 'ADD_LIST',
      DELETE_LIST: 'DELETE_LIST',
   },
   CARDS: {
      ADD_CARD: 'ADD_CARD',
      UPDATE_CARD: 'UPDATE_CARD',
   },
};

export default ACTION_TYPES;
