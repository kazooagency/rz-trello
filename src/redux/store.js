import { createStore } from 'redux';
import trelloApp from './reducers';
import { loadState, saveState } from './localStorage';

const persistedState = loadState();
console.log(persistedState);
const store = createStore(trelloApp, persistedState);
store.subscribe(() => {
   saveState(store.getState());
   console.log('state ', store.getState());
});

// const store = createStore(trelloApp);

export default store;
