import React, { useState } from 'react';
import { connect } from 'react-redux';
import { StyleSheet, css } from 'aphrodite';

import AddButton from './components/AddButton';
import List from './components/List';
import CardDetails from './components/CardDetails';

import { addList, addCard, deleteList, updateCard } from './redux/actions';

const App = props => {
   const { lists, dispatch } = props;
   const [isOpen, setIsOpen] = useState(true);
   const [currentCardId, setCurrentCardId] = useState(0);

   const handleCardClick = id => {
      setCurrentCardId(id);
      setIsOpen(true);
   };

   const getCardData = cardId => {
      console.log(cardId);
      console.log(lists);
      let cardData = null;
      lists.map(list => {
         list.cards.map(card => {
            if (card.id === cardId) {
               cardData = card;
            }
         });
      });
      return cardData;
   };

   const handleDialogClose = () => {
      setIsOpen(false);
   };

   const handleDialogValidate = card => {
      dispatch(updateCard(card));
   };
   return (
      <div className={css(styles.app)}>
         <div className={css(styles.container)}>
            {getCardData(currentCardId) ? (
               <CardDetails
                  isOpen={isOpen}
                  title={getCardData(currentCardId).text}
                  description={getCardData(currentCardId).description}
                  onClose={handleDialogClose}
                  cardId={currentCardId}
                  onValidate={card => handleDialogValidate(card)}
               />
            ) : null}
            {lists
               ? lists.map(list => (
                    <div className={css(styles.listAndAddButtonConter)}>
                       <List
                          listID={list.id}
                          title={list.title}
                          cards={list.cards}
                          cardOnClick={cardId => handleCardClick(cardId)}
                          deleteList={() => {
                             if (
                                window.confirm('Êtes-vous sûr de vouloir supprimer cette liste ?')
                             ) {
                                dispatch(deleteList(list.id));
                             }
                          }}
                       />
                       <AddButton
                          action={cardText => dispatch(addCard(list.id, cardText))}
                          type="card"
                          another={list.cards.length && list.cards.length > 0}
                       />
                    </div>
                 ))
               : null}
            <AddButton
               action={listTile => dispatch(addList(listTile))}
               type="list"
               another={lists && lists.length && lists.length > 0}
            />
         </div>
      </div>
   );
};

const mapStateToProps = state => ({
   lists: state,
});

const ConnectedApp = connect(mapStateToProps)(App);

export default ConnectedApp;

const styles = StyleSheet.create({
   app: {},
   container: {
      display: 'flex',
   },
   listAndAddButtonConter: {
      backgroundColor: '#ebecf0',
      maxWidth: '272px',
      padding: '8px',
      marginRight: '8px',
      borderRadius: '3px',
   },
});
